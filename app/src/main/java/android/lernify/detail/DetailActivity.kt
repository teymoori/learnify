package android.lernify.detail

import android.lernify.R
import android.lernify.utils.entities.ArticleModel
import android.lernify.utils.loadFromURL
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {
    lateinit var article: ArticleModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        article = intent.getParcelableExtra("article");

        cover.loadFromURL(article.timeLinePhoto)
        articleTitle.text = article.fullTitle
//        title. = items[position].fullTitle
//        question.text = items[position].shortQuestion
//        cover.loadFromURL(items[position].timeLinePhoto)

    }

}
