package android.lernify.storyBoard

import android.content.Intent
import android.lernify.R
import android.lernify.detail.DetailActivity
import android.lernify.utils.base.BaseActivity
import android.lernify.utils.base.Gen
import android.lernify.utils.base.RestHandler
import android.lernify.utils.base.RestHandler.Companion.loading
import android.lernify.utils.entities.ArticleModel
import android.lernify.utils.toastError
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_story_baord.*
import androidx.recyclerview.widget.PagerSnapHelper

class StoryBoardActivity : BaseActivity()  {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story_baord)
        val viewModel = ViewModelProviders.of(this).get(StoryBoardViewModel::class.java)
        observeMessages(viewModel)
    }


    private fun observeMessages(viewModel: StoryBoardViewModel) {
        viewModel.getArticles().observe(this, Observer {
            when (it?.status) {
                RestHandler.Status.LOADING -> loading(true)
                RestHandler.Status.ERROR -> {
                    it.exception?.toastError()
                    loading(false)
                }
                RestHandler.Status.SUCCESS -> {
                    loading(false)
//                    fillValues(it.data)
                  //  Gen.toast(it.data.toString())
                    val adapter = it.data?.let { items -> StoryBoardListAdapter(items , mActivity) }
                    articlesView.adapter = adapter
                    val snapHelper = PagerSnapHelper()
                    snapHelper.attachToRecyclerView(articlesView)
                }
            }
        })
    }



}
