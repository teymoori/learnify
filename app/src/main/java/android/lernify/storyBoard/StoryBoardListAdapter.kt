package android.lernify.storyBoard

import android.app.Activity
import android.app.ActivityOptions
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.lernify.R
import android.lernify.detail.DetailActivity
import android.lernify.utils.base.Gen
import android.lernify.utils.base.MyApplication
import android.lernify.utils.customViews.MyTextView
import android.lernify.utils.entities.ArticleModel
import android.lernify.utils.loadFromURL
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.palette.graphics.Palette
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.story_board_item.view.*
import android.R.string
import android.util.Log
import java.lang.Integer.parseInt


class StoryBoardListAdapter(private val items: List<ArticleModel>, private val mActivity: Activity) :
    androidx.recyclerview.widget.RecyclerView.Adapter<StoryBoardListAdapter.Holder>() {
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.story_board_item, parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.title.text = items[position].fullTitle
        holder.question.text = items[position].shortQuestion
        holder.cover.loadFromURL(items[position].timeLinePhoto)
        holder.cover.setOnClickListener {
            onStorySelect(items[position], holder)
        }

        //title color
        if (items[position].titleColor.isNotEmpty()) {
            Log.d("color_", items[position].titleColor);
            holder.title.setTextColor(Color.parseColor(items[position].titleColor))
        } else {
            createPaletteAsync(items[position].timeLinePhoto, holder)
        }


    }

    override fun getItemCount(): Int {
        return items.size
    }


    class Holder(private val view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        val title: MyTextView = view.title
        val question: MyTextView = view.question
        val cover: ImageView = view.cover
    }

    private fun onStorySelect(
        article: ArticleModel,
        holder: Holder
    ) {
        val intentMessages = Intent(mActivity, DetailActivity::class.java).putExtra("article", article)
        val pairs0: android.util.Pair<View?, String?> =
            android.util.Pair<View?, String?>(holder.cover, "imageTransition")
        val pairs1: android.util.Pair<View?, String?> =
            android.util.Pair<View?, String?>(holder.title, "titleTransition")
        val pairs: Array<android.util.Pair<View?, String?>> = arrayOf(pairs0, pairs1)
        val activityOptions: ActivityOptions = ActivityOptions.makeSceneTransitionAnimation(mActivity, *pairs)
        mActivity.startActivity(intentMessages, activityOptions.toBundle())
    }

    private fun createPaletteAsync(imgURL: String, holder: Holder) {
        Thread(Runnable {
            val bitmap = Glide.with(mActivity)
                .asBitmap()
                .load(imgURL)
                .submit().get()
            Palette.from(bitmap).generate { palette ->
                mActivity.runOnUiThread(Runnable {
                    holder.title.setTextColor(palette?.getLightVibrantColor(Color.WHITE) ?: Color.WHITE)
                })
            }
        }).start()
    }


}