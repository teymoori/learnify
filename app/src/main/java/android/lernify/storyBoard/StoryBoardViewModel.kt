package android.lernify.storyBoard

import android.lernify.utils.RestRepository
import android.lernify.utils.RxDisposableManager
import android.lernify.utils.base.RestHandler
import android.lernify.utils.entities.ArticleModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel

class StoryBoardViewModel : ViewModel() {

    override fun onCleared() {
        super.onCleared()
         RxDisposableManager.getCompositeDisposable().dispose()
    }

    private var articles: LiveData<RestHandler<List<ArticleModel>>>? = null
    fun getArticles(): LiveData<RestHandler<List<ArticleModel>>> = articles
            ?: RestRepository.instance.getArticles().also {
                articles = it
            }

}