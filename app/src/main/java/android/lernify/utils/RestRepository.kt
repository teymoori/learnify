package android.lernify.utils

import android.lernify.utils.base.Gen
import android.lernify.utils.base.RestClient
import android.lernify.utils.base.RestHandler
import android.lernify.utils.entities.ArticleModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RestRepository {

    companion object {
        val instance = RestRepository()
    }

    fun getArticles(): LiveData<RestHandler<List<ArticleModel>>> {
        val data: MutableLiveData<RestHandler<List<ArticleModel>>> = MutableLiveData()
        val disposable = RestClient.service(false).getArticles().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { data.value = RestHandler.loading(null) }
                .subscribe({ result ->
                    if (result.isSuccessful) {
                        if (result.body() != null) {
                            data.value = RestHandler.success(result.body())
                        }
                    } else {
                        data.value = RestHandler.error("error")
                    }
                }, { error ->
                    data.value = RestHandler.error(error.message)
                })
        RxDisposableManager.getCompositeDisposable().add(disposable)
        return data
    }
//
//
//    fun getInvoiceList(unitID: String): LiveData<RestHandler<List<InvoiceModel>>> {
//        val data: MutableLiveData<RestHandler<List<InvoiceModel>>> = MutableLiveData()
//        val disposable = RestClient.service(true).getInvoicesOfUnit(unitID).observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io())
//                .doOnSubscribe { data.value = RestHandler.loading(null) }
//                .subscribe({ result ->
//                    if (result.isSuccessful) {
//                        if (result.body() != null) {
//                            data.value = RestHandler.success(result.body())
//                        } else {
//                            val error = Gen.getError(result.errorBody())
//                            data.value = RestHandler.error(error.status.toString() + " " + error.detail)
//                        }
//                    } else
//                        data.value = RestHandler.error(Gen.getStringRes(R.string.error))
//                }, { error ->
//                    data.value = RestHandler.error(error.message)
//                })
//        RxDisposableManager.getCompositeDisposable().add(disposable)
//        return data
//    }
//
//
//    fun getInvoiceDetail(params: HashMap<String, String>): LiveData<RestHandler<InvoiceModel>> {
//        val data: MutableLiveData<RestHandler<InvoiceModel>> = MutableLiveData()
//        val disposable = RestClient.service(true).getInvoiceDetailByParameters(params).observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io()).subscribe({ result ->
//                    if (result.isSuccessful) {
//                        if (result.body() != null) {
//                            result.body()?.get(0)?.let { data.value = RestHandler.success(it) }
//                        } else {
//                            val error = Gen.getError(result.errorBody())
//                            data.value = RestHandler.error(error.status.toString() + " " + error.detail)
//                        }
//                    } else
//                        data.value = RestHandler.error("${result.code()} ${result.message()}")
//                }, { error ->
//                    data.value = RestHandler.error(error.message)
//                })
//        RxDisposableManager.getCompositeDisposable().add(disposable)
//        return data
//    }
//
//
//    fun updateProfile(user: UpdateUserRequestModel): LiveData<RestHandler<UserDetailModel>> {
//
//        val data: MutableLiveData<RestHandler<UserDetailModel>> = MutableLiveData()
//        val disposable = RestClient.service(true).updateUser(user).observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io()).subscribe({ result ->
//                    if (result.isSuccessful) {
//                        if (result.body() != null) {
//                            result.body()?.let { data.value = RestHandler.success(it) }
//                        } else {
//                            val error = Gen.getError(result.errorBody())
//                            data.value = RestHandler.error(error.status.toString() + " " + error.detail)
//                        }
//                    } else
//                        data.value = RestHandler.error("${result.code()} ${result.message()}")
//                }, { error ->
//                    data.value = RestHandler.error(error.message)
//                })
//        RxDisposableManager.getCompositeDisposable().add(disposable)
//        return data
//    }
//
//
//    fun getSubscriptionsList(): LiveData<RestHandler<List<SubscribeResponseModel>>> {
//        val data: MutableLiveData<RestHandler<List<SubscribeResponseModel>>> = MutableLiveData()
//        Gen.getUserName()?.let {
//            val disposable = RestClient.service(true).getUserSubscriptions().observeOn(AndroidSchedulers.mainThread())
//                    .subscribeOn(Schedulers.io()).subscribe({ result ->
//                        if (result.isSuccessful) {
//                            if (result.body() != null) {
//                                result.body()?.let { data.value = RestHandler.success(it) }
//                            } else {
//                                val error = Gen.getError(result.errorBody())
//                                data.value = RestHandler.error(error.status.toString() + " " + error.detail)
//                            }
//                        } else
//                            data.value = RestHandler.error("${result.code()} ${result.message()}")
//                    }, { error ->
//                        data.value = RestHandler.error(error.message)
//                    })
//            RxDisposableManager.getCompositeDisposable().add(disposable)
//        }
//        return data
//    }
//
//
//    fun getServiceGroupsList(): LiveData<RestHandler<List<ServiceGroupModel>>> {
//        val data: MutableLiveData<RestHandler<List<ServiceGroupModel>>> = MutableLiveData()
//        val disposable = RestClient.service(true).getServiceGroups().observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io())
//                .subscribe({ result ->
//                    if (result.isSuccessful) {
//                        if (result.body() != null) {
//                            result.body()?.let {
//                                data.value = RestHandler.success(it)
//                                cacheGroupsToDB(it)
//                            }
//                        } else {
//                            val error = Gen.getError(result.errorBody())
//                            data.value = RestHandler.error(error.status.toString() + " " + error.detail)
//                        }
//                    } else
//                        data.value = RestHandler.error("${result.code()} ${result.message()}")
//                }, { error ->
//                    data.value = RestHandler.error(error.message)
//                })
//        RxDisposableManager.getCompositeDisposable().add(disposable)
//        return data
//    }
//
//    private fun cacheGroupsToDB(groups: List<ServiceGroupModel>) {
//        val serviceRepo = DBServiceGroupsRepository()
//        serviceRepo.insertAll(groups)
//    }
//
//
//    fun getServiceUnitsList(id: Long): LiveData<RestHandler<List<ServiceUnitModel>>> {
//        val data: MutableLiveData<RestHandler<List<ServiceUnitModel>>> = MutableLiveData()
//        val disposable = RestClient.service(true).getServiceUnits(id.toString()).observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io()).subscribe({ result ->
//                    if (result.isSuccessful) {
//                        if (result.body() != null) {
//                            result.body()?.let {
//                                data.value = RestHandler.success(it)
//                            }
//                        } else {
//                            val error = Gen.getError(result.errorBody())
//                            data.value = RestHandler.error(error.status.toString() + " " + error.detail)
//                        }
//                    } else
//                        data.value = RestHandler.error("${result.code()} ${result.message()}")
//                }, { error ->
//                    data.value = RestHandler.error(error.message)
//                })
//        RxDisposableManager.getCompositeDisposable().add(disposable)
//        return data
//    }
//
//    fun getRolesByServiceGroupType(serviceGroupTypeId: Long): LiveData<RestHandler<List<RoleModel>>> {
//        val data: MutableLiveData<RestHandler<List<RoleModel>>> = MutableLiveData()
//        val disposable = RestClient.service(true).getRoles(serviceGroupTypeId).observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io()).subscribe({ result ->
//                    if (result.isSuccessful) {
//                        if (result.body() != null) {
//                            result.body()?.let {
//                                data.value = RestHandler.success(it)
//                            }
//                        } else {
//                            val error = Gen.getError(result.errorBody())
//                            data.value = RestHandler.error(error.status.toString() + " " + error.detail)
//                        }
//                    } else
//                        data.value = RestHandler.error("${result.code()} ${result.message()}")
//                }, { error ->
//                    data.value = RestHandler.error(error.message)
//                })
//        RxDisposableManager.getCompositeDisposable().add(disposable)
//        return data
//    }
//
//
//    fun subscribeRequest(serviceUnitID: Long, roleID: Long): LiveData<RestHandler<SubscribeResponseModel>> {
//        val data: MutableLiveData<RestHandler<SubscribeResponseModel>> = MutableLiveData()
//
//        Gen.getUserName()?.let {
//            val reqModel = SubscribeRequestModel(userName = it, subscriberRoleId = roleID, serviceUnitId = serviceUnitID)
//            val disposable = RestClient.service(true).subscribeRequest(reqModel).observeOn(AndroidSchedulers.mainThread())
//                    .subscribeOn(Schedulers.io()).subscribe({ result ->
//                        if (result.isSuccessful) {
//                            if (result.body() != null) {
//                                result.body()?.let {
//                                    data.value = RestHandler.success(it)
//                                }
//                            } else {
//                                val error = Gen.getError(result.errorBody())
//                                data.value = RestHandler.error(error.status.toString() + " " + error.detail)
//                            }
//                        } else {
//                            val error = Gen.getError(result.errorBody())
//                            data.value = RestHandler.error(error.status.toString() + error.detail)
//                        }
//                    }, { error ->
//                        data.value = RestHandler.error(error.message)
//                    })
//            RxDisposableManager.getCompositeDisposable().add(disposable)
//        }
//        return data
//    }




}