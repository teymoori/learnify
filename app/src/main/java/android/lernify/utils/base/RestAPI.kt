package android.lernify.utils.base


import android.lernify.utils.entities.ArticleModel
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*


interface RestAPI {

    @GET("5cc5c4842e00005000594afb")
    fun getArticles():
            Observable<Response<List<ArticleModel>>>

}