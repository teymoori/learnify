package android.lernify.utils.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ArticleModel(
    val articleSource: String,
    val author: String,
    val date: String,
    val firstPhoto: String,
    val fullContent: String,
    val fullTitle: String,
    val likes: Int,
    val reads: Int,
    val secondPhoto: String,
    val shortContent: String,
    val shortQuestion: String,
    val shortTitle: String,
    val tags: String,
    val timeLinePhoto: String,
    val timeRead: String,
    val video: String,
    val titleColor: String
): Parcelable